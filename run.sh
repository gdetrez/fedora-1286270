#!/bin/sh

VERSIONS=$(seq 20 26)

for version in ${VERSIONS}; do
  docker build -t "bug1286270:f${version}" "f${version}"
done

for version in ${VERSIONS}; do
  printf "\nFedora %s:\n" "$version"
  for LC_PAPER in "" en_US.UTF-8 en_GB.UTF-8; do
    printf "\tLC_PAPER=%s: " "$LC_PAPER"
    docker run --rm -ti -e "LC_PAPER=$LC_PAPER" "bug1286270:f${version}"
  done
done
